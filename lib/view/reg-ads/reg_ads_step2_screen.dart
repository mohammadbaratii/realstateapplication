import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/component/components.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/enums/snackbar_type.dart';
import 'package:rahnamaye_man/core/util/util.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/model/key_value.dart';
import 'package:rahnamaye_man/viewModel/reg_ads_step2_view_model.dart';

class RegAdsStep2Screen extends BaseStatelessWidget {
  late RegAdsStep2ViewModel vm;

  late KeyValue bargain;

  RegAdsStep2Screen(this.bargain);

  @override
  Widget build(BuildContext context) {
    vm = Get.put(RegAdsStep2ViewModel());

    vm.setBargainId(bargain.id!);

    return GetBuilder<RegAdsStep2ViewModel>(
        builder: (value) => Scaffold(
              backgroundColor: AppColors.primary,
              appBar: appBar(AppStrings.REG_YOUR_ESTATE_ADS),
              body: Card(
                color: AppColors.background,
                elevation: 0,
                margin: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                )),
                child: Column(
                  children: [
                    Expanded(
                      child: Padding(
                          padding: const EdgeInsets.all(Dimens.rootPadding),
                          child: vm.formMaker.make(onUpdate: () {
                            vm.update();
                          }, onValidate: () {
                            snackBar("Success call api", type: SnackBarType.SUCCESS);
                          }, onContinue: (value) {
                            vm.fetchForm(bargain.id, value);
                          })),
                    ),
                    primaryButton(AppStrings.NEXT_IN_REG_ADS, () {
                      vm.formMaker.validate();
                    })
                  ],
                ),
              ),
            ));
  }
}
