import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/viewModel/search_view_model.dart';

class SearchScreen extends BaseStatelessWidget {
  late SearchViewModel vm;
  var _fileCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    vm = Get.put(SearchViewModel());

    return GetBuilder<SearchViewModel>(
      builder: (value) => Scaffold(
        backgroundColor: AppColors.primary,
        appBar: appBar(AppStrings.SEARCH_TITLE),
        body: Container(
          height: double.infinity,
          child: Card(
            color: AppColors.background,
            elevation: 0,
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            )),
            child: Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 16,
                    ),
                    Image.asset(
                      'assets/images/search_header.png',
                      width: 300,
                      height: 300,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        elevation: 8.0,
                        child: TextField(
                          controller: _fileCodeController,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                              hintText: AppStrings.SEARCH_HINT,
                              counterText: '',
                              border: InputBorder.none,
                              hintStyle: TextStyle(color: AppColors.textColor2),
                              prefixIcon: Icon(
                                Icons.search,
                                color: AppColors.textColor2,
                              )),
                          maxLength: 4,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 8.0, right: 12, left: 12, bottom: 12),
                      child: SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppStrings.SEARCH,
                              style: TextStyle(fontSize: Dimens.textSize3),
                            ),
                          ),
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.primary,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}