import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';
import 'package:rahnamaye_man/viewModel/messages_view_model.dart';
import 'package:rahnamaye_man/viewModel/requests_view_model.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class RequestsScreen extends BaseStatelessWidget {
  late RequestsViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(RequestsViewModel());

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: AppColors.orange));

    return GetBuilder<RequestsViewModel>(
        builder: (builder) => SafeArea(
                child: Scaffold(
              appBar: AppBar(
                elevation: 0,
                backgroundColor: AppColors.orange,
                title: Text('درخواست های من'),
                centerTitle: true,
              ),
              body: Column(
                children: [
                  ShapeOfView(
                    shape: ArcShape(direction: ArcDirection.Outside, height: 25, position: ArcPosition.Bottom),
                    child: Container(
                      width: double.infinity,
                      color: AppColors.orange,
                      child: Column(
                        children: [
                          ClipOval(
                            child: Image.network(
                              'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
                              height: 100.0,
                              width: 100.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'احمد کاشانی',
                              style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize2),
                            ),
                          ),
                          Text(
                            'متخصص فنی لوله کش',
                            style: TextStyle(color: AppColors.white, fontSize: Dimens.textSize5),
                          ),
                          SizedBox(height: 12),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 8),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: 5,
                      itemBuilder: (context, index) => _item(),
                    ),
                  ),
                ],
              ),
            )));
  }

  Widget _item() {
    return Padding(
      padding: const EdgeInsets.only(left: 12, right: 12, top: 4),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'عنوان: اجاره آپارتمان 100 متری بلوار خاقانی',
                style: TextStyle(fontSize: Dimens.textSize4, color: AppColors.textColor1, height: 2),
              ),
              Text(
                'منطقه موردنظر: تهران | ورامین (خیابان حامدی)',
                style: TextStyle(fontSize: Dimens.textSize4, color: AppColors.textColor2, height: 2),
              ),
              Text(
                'تاریخ درخواست: 1400/10/13',
                style: TextStyle(fontSize: Dimens.textSize5, color: AppColors.textColor3, height: 2),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
