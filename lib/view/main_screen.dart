import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/component/panda_bar/panda_bar.dart';
import 'package:rahnamaye_man/component/panda_bar/panda_bar_button_data.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/view/commission_screen.dart';
import 'package:rahnamaye_man/view/home_screen.dart';
import 'package:rahnamaye_man/view/profile/profile_screen.dart';
import 'package:rahnamaye_man/view/reg-ads/reg_ads_screen.dart';
import 'package:rahnamaye_man/view/search_screen.dart';
import 'package:rahnamaye_man/viewModel/main_view_model.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class MainScreen extends BaseStatelessWidget {
  late MainViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(MainViewModel());

    vm.pageController = PageController(initialPage: vm.currentPage);

    return FocusDetector(
      onVisibilityGained: () {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: AppColors.secondary));
      },
      child: GetBuilder<MainViewModel>(
          builder: (value) => Scaffold(
              bottomNavigationBar: PandaBar(
                backgroundColor: AppColors.white,
                buttonSelectedColor: AppColors.primary,
                fabColors: [
                  AppColors.secondary,
                  AppColors.primary,
                ],
                buttonData: [
                  PandaBarButtonData(id: 0, icon: Icons.home_rounded, title: AppStrings.HOME),
                  PandaBarButtonData(id: 1, icon: Icons.search_rounded, title: AppStrings.SEARCH),
                  PandaBarButtonData(
                      id: 2, icon: Icons.calculate_rounded, title: AppStrings.COMMISSION),
                  PandaBarButtonData(
                      id: 3, icon: Icons.person_pin_rounded, title: AppStrings.PROFILE),
                ],
                onChange: (id) {
                  vm.setCurrentPage(id);
                },
                onFabButtonPressed: () => Get.to(RegAdsScreen()),
              ),
              body: Stack(
                children: [
                  PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: vm.pageController,
                    children: <Widget>[
                      HomeScreen(),
                      SearchScreen(),
                      CommissionScreen(),
                      ProfileScreen(),
                    ],
                  ),
                ],
              ))),
    );
  }
}
