import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_stateless_widget.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/strings.dart';
import 'package:rahnamaye_man/core/values/styles.dart';
import 'package:rahnamaye_man/viewModel/commission_view_model.dart';
import 'package:rahnamaye_man/viewModel/home_view_model.dart';
import 'package:rahnamaye_man/viewModel/search_view_model.dart';

class CommissionScreen extends BaseStatelessWidget {
  late CommissionViewModel vm;

  @override
  Widget build(BuildContext context) {
    vm = Get.put(CommissionViewModel());

    return GetBuilder<CommissionViewModel>(
      builder: (value) => Scaffold(
        backgroundColor: AppColors.primary,
        appBar: appBar(AppStrings.COMMISSION_TITLE),
        body: Card(
          color: AppColors.background,
          elevation: 0,
          margin: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          )),
          child: Container(),
        ),
      ),
    );
  }
}
