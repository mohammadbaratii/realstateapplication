class KeyValue {
  dynamic id;
  String? title;
  bool selected = false;

  KeyValue({this.id, this.title, required this.selected});

  factory KeyValue.fromJson(Map<String, dynamic> json) {
    return KeyValue(
      id: json['id'],
      title: json['title'],
      selected: false,
    );
  }
}
