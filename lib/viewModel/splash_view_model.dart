import 'package:get/get.dart';
import 'package:rahnamaye_man/core/base/base_view_model.dart';
import 'package:rahnamaye_man/core/data/repository/user_repository.dart';
import 'package:rahnamaye_man/view/entrance/login_screen.dart';
import 'package:rahnamaye_man/view/main_screen.dart';

class SplashViewModel extends BaseViewModel {
  goNextScreen() {
    if (UserRepository().isLogin()) {
      Get.offAll(MainScreen());
    } else {
      Get.offAll(LoginScreen());
    }
  }
}
