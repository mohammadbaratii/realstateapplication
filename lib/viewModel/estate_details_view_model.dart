import 'package:rahnamaye_man/core/base/base_view_model.dart';

class EstateDetailsViewModel extends BaseViewModel{

  int currentSlide = 0;

  setSliderIndex(int index){
    currentSlide = index;

    update();
  }
}