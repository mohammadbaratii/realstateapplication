import 'package:rahnamaye_man/model/field.dart';

class FiltersRequest {
  String bargainId;
  String propertyId;

  FiltersRequest({required this.bargainId, required this.propertyId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bargain_id'] = this.bargainId;
    data['property_id'] = this.propertyId;

    return data;
  }
}

class FiltersResponse {
  List<Field> form = [];

  FiltersResponse({required this.form});

  factory FiltersResponse.fromJson(Map<String, dynamic> json) {
    List<Field> form = [];

    if (json['result'] != null) {
      (json['result'] as List).forEach((element) => form.add(Field.fromJson(element)));
    }

    return FiltersResponse(form: form);
  }
}
