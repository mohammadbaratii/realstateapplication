import 'package:rahnamaye_man/model/key_value.dart';

class BargainsRequest {
  int cityId;

  BargainsRequest({required this.cityId});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city_id'] = this.cityId;

    return data;
  }
}

class BargainsResponse {
  List<KeyValue> bargains = [];

  BargainsResponse({required this.bargains});

  factory BargainsResponse.fromJson(Map<String, dynamic> json) {
    List<KeyValue> bargains = [];

    (json['result'] as List).forEach((element) => bargains.add(KeyValue.fromJson(element)));

    return BargainsResponse(bargains: bargains);
  }
}
