import 'package:rahnamaye_man/core/api/model/api_error.dart';

class BaseResponse {
  String status;
  List<ApiError>? errors = [];
  String version;

  BaseResponse({
    required this.status,
    required this.errors,
    required this.version,
  });

  factory BaseResponse.fromJson(Map<String, dynamic> json) {
    //<editor-fold desc="parse errors">
    List<ApiError> errors = [];

    if(json['errors'] is List) {
      (json['errors'] as List).forEach((element) {
        errors.add(ApiError.fromJson(element));
      });
    }
    //</editor-fold>

    return BaseResponse(
      status: json['status'],
      errors: errors,
      version: json['version'],
    );
  }
}
