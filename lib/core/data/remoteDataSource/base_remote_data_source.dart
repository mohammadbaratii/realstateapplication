import 'package:dio/dio.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' as getX;
import 'package:rahnamaye_man/core/api/model/base_response.dart';
import 'package:rahnamaye_man/core/enums/snackbar_type.dart';
import 'package:rahnamaye_man/core/util/util.dart';
import 'package:rahnamaye_man/view/entrance/login_screen.dart';

class BaseRemoteDataSource {
  Dio _dio = new Dio();

  ApiErrorCallback? _onError;

  BaseRemoteDataSource getDio() {
    BaseRemoteDataSource instance = BaseRemoteDataSource();

    if (!instance._dio.options.headers.containsKey('token')) {
      //<editor-fold desc="headers">
      instance._dio.options.headers = {
        'token': 'ea1c4dc405e31f5406f8dee474b2fe2e'
      };
      //</editor-fold>
    }
    return instance;
  }

  BaseRemoteDataSource() {
    _dio.interceptors.add(LogInterceptor());
    _dio.options.connectTimeout = 10000;
    _dio.options.receiveTimeout = 10000;
  }

  Future<Response> post<T>(String path,
      {dynamic data,
      Parser<T>? parser,
      ApiSuccessCallback<T>? onSuccess,
      ApiErrorCallback? onError,
      Map<String, dynamic>? queryParameters}) async {
    _onError = onError;

    _setInterceptors(parser, onSuccess);

    return await _dio.post(path, data: data, queryParameters: queryParameters);
  }

  _setInterceptors<T>(
    Parser<T>? parser,
    ApiSuccessCallback<T>? onSuccess,
  ) {
    _dio.interceptors.add(
        InterceptorsWrapper(onRequest: (RequestOptions options, handler) async {
      // final isOnline = await Util.isOnline();
      // if (!isOnline) DialogUtil.showIsOfflineDialog();

      return handler.next(options); //continue
    }, onError: (DioError e, handler) async {
      EasyLoading.dismiss(animation: true);

      //<editor-fold desc="500">
      if (e.error.toString().contains('500')) {
        if (_onError != null) {
          _onError!.call(error: e);
          // alert('خطا', 'سرور خوابیده!', icon: 'assets/icons/warning.svg');
        }
      }
      //</editor-fold>

      //<editor-fold desc="401">
      if (e.response != null && e.response!.statusCode == 401) {
        getX.Get.to(LoginScreen());

        return;
      }
      //</editor-fold>

      if (_onError != null) {
        _onError!.call(error: e);
      }
    }, onResponse: (Response r, handler) {
      if (noProblem(r)) {
        if (onSuccess != null) {
          if (parser != null) {
            onSuccess.call(parser.call(r.data['result']));
          } else {
            /*if (r.data['result'] != null) {
              onSuccess.call(r.data['result']);
            } else {
              onSuccess.call(r.data);
            }*/
          }
        }
      }
    }));
  }

  bool noProblem(Response<dynamic> response) {
    EasyLoading.dismiss(animation: true);

    BaseResponse? res;

    if (response.statusCode == 200) {
      res = BaseResponse.fromJson(response.data);

      if (res.status != 'success') {
        if (_onError != null) {
          _onError!.call(
            response: res,
          );

          if (res.errors!.isNotEmpty) {
            snackBar(res.errors![0].message!, type: SnackBarType.ERROR);
          }
        }
      }

      return res.status == 'success';
    } else {
      if (_onError != null) {
        _onError!.call(response: res!);
      }
      return false;
    }
  }
}

typedef ApiSuccessCallback<T> = void Function(T response);

typedef ApiErrorCallback = void Function(
    {DioError? error, BaseResponse? response});

typedef Parser<T> = T Function(dynamic);
