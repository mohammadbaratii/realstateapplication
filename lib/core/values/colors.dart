import 'dart:ui';

class AppColors{

  static const Color primary = Color(0xFF3C0AE5);
  static const Color secondary = Color(0xFF750DE6);
  static const Color background = Color(0xFFF5F5F5);

  static const Color bottomNavigationItemSelected = Color(0xFFFFFFFF);
  static const Color bottomNavigationItemUnselected = Color(0xFFEEEEEE);

  static const Color textColor1 = Color(0xFF262626);
  static const Color textColor2= Color(0xFF878787);
  static const Color textColor3= Color(0xFFABABAB);

  static const Color white = Color(0xFFFFFFFF);
  static const Color gray = Color(0xFFB8B8B8);
  static const Color blue = Color(0xFF47B8FC);
  static const Color orange = Color(0xFFFA7E3F);
  static const Color green = Color(0xFF1FC08C);
  static const Color black = Color(0xFF222222);
  static const Color yellow = Color(0xFFF2D265);
  static const Color red = Color(0xFFE04F5F);
  static const Color purple = Color(0xFF90A2F9);
}