class Dimens{
  static const double textSize1 = 20;
  static const double textSize2 = 18;
  static const double textSize3 = 16;
  static const double textSize4 = 14;
  static const double textSize5 = 12;
  static const double textSize6 = 10;

  static const double rootPadding = 16;
  static const double iconSize = 32;
  static const double buttonRadius = 0;
  static const double fieldRadius = 24;
}