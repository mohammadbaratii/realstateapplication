import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:get/get_navigation/src/snackbar/snack.dart';
import 'package:rahnamaye_man/core/enums/snackbar_type.dart';
import 'package:rahnamaye_man/core/values/colors.dart';

snackBar(String message, {String? title, SnackBarType? type}) {
  Color color;

  switch (type ?? SnackBarType.ERROR) {
    case SnackBarType.SUCCESS:
      color = AppColors.green;
      break;

    case SnackBarType.WARNING:
      color = AppColors.orange;
      break;

    case SnackBarType.ERROR:
      color = AppColors.red;
      break;
  }

  Get.snackbar(
    title ?? '',
    message,
    backgroundColor: color,
    colorText: AppColors.white,
    // dismissDirection: SnackDismissDirection.VERTICAL,
    margin: EdgeInsets.all(0),
    borderRadius: 0,
    // icon: Icon(Icons.warning, color: Colors.white),
  );
}
