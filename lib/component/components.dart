//<editor-fold desc="primaryButton">
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rahnamaye_man/core/values/colors.dart';
import 'package:rahnamaye_man/core/values/dimens.dart';

Widget primaryButton(String title, VoidCallback onTap, {String? icon}) => ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: AppColors.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.buttonRadius),
        ),
        minimumSize: Size(double.infinity, 30), // double.infinity is the width and 30 is the height
      ),
      onPressed: onTap,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon == null ? 'assets/icons/arrow_right.svg' : icon, //fixme
              width: Dimens.iconSize,
              height: Dimens.iconSize,
              color: AppColors.white,
            ),
            SizedBox(width: 8),
            Text(
              title,
              style: TextStyle(
                  color: AppColors.white,
                  fontSize: Dimens.textSize3,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
      ),
    );
//</editor-fold>
